import { createApp } from "vue";
import App from "./App.vue";
import store from "./store";
// import Vue from 'vue';

// Vue.config.productionTip = false;

// new Vue({
//     render: h => h(App),
//     store
// }).$mount('#app')

createApp(App).use(store).mount("#app");
