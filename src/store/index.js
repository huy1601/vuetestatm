import { createStore } from "vuex";
import axios from "axios";

const store = createStore({
  state: {
    data: [],
  },

  getter: {
    data(state) {
      return state.data;
    },
  },

  mutations: {
    setData(state, data) {
      state.data = data;
      return state.data;
    },

    ADD_ATM(state) {
      // console.log("atm: ", atm);
      // const newAtm = state.data.concat(atm);
      // state.data = newAtm;
      // console.log("NewATM1: ", newAtm);

      return state.data;
    },

    DELETE_ATM(state, atmId) {
      const index = state.data.atm.findIndex((a) => a.id != atmId);
      state.data.atm.splice(index, 1);
    },
  },
  actions: {
    async AxiosData({ commit }) {
      const response = await axios.get("http://127.0.0.1:8091/api/v1/atms", {
        headers: {
          Authorization:
            "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjMwN2ZhZDYyYTIxZmM1NDNiODM0MTAiLCJpYXQiOjE2NDczNDYzMjB9.gj9g1tdaTnU-5gqdzht7jMVhDRURyg9oIgVv922rEuY",
          Accept: "application/json",
          "content-Type": "application/json",
        },
      });
      const data = response.data;
      console.log("data: ", data);
      commit("setData", data);
    },

    async DeleteAtm({ commit }, atm) {
      await axios
        .delete(`http://localhost:8091/api/v1/atms/${atm.id}`, {
          headers: {
            Authorization:
              "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjMwN2ZhZDYyYTIxZmM1NDNiODM0MTAiLCJpYXQiOjE2NDczNDYzMjB9.gj9g1tdaTnU-5gqdzht7jMVhDRURyg9oIgVv922rEuY",
            Accept: "application/json",
            "content-Type": "application/json",
          },
        })
        .then(() => {
          commit("DELETE_ATM", atm.id);
        });
    },

    async addATM({ commit }, atm) {
      const response = await axios.post(
        "http://localhost:8091/api/v1/atms",
        { name: atm },
        {
          headers: {
            Authorization:
              "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjMwN2ZhZDYyYTIxZmM1NDNiODM0MTAiLCJpYXQiOjE2NDczNDYzMjB9.gj9g1tdaTnU-5gqdzht7jMVhDRURyg9oIgVv922rEuY",
            Accept: "application/json",
            "content-Type": "application/json",
          },
        }
      );
      const newATM = response.data;
      console.log("newATM: ", newATM);
      commit("ADD_ATM", newATM);
    },
  },
});

export default store;
